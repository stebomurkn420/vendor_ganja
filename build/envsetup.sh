# hmm() from platform build/make/envsetup.sh
HMM=$(hmm)

function hmm() {
echo "$HMM"
cat <<EOF
Functions from vendor/ganja/build/envsetup.sh:
reposync:       repo syncs using some 'optimizations' and more make threads
build:          lunches and makes with 1.5x CPU threads. EX: build marlin
EOF
}

function getmakeflags() {
    SOCKETS=$(lscpu | sed -n 's/Socket(s):.* //p')
    CORES=$((SOCKETS * $(lscpu | sed -n 's/Core(s) per socket:.* //p')))
    THREADS=$((CORES * $(lscpu | sed -n 's/Thread(s) per core:.* //p')))
    MAKEFLAGS=$((THREADS * 150 / 100))
    echo "$MAKEFLAGS"
}

function reposync() {
    repo sync -j $(getmakeflags) \
            --no-clone-bundle --prune --no-tags -c --optimized-fetch -f "$@"
}

function build() {
    PRODUCT=$1
    THREADS=$(getmakeflags)
    if [[ ! $PRODUCT =~ ganja_  && ! $PRODUCT =~ - ]]; then
        lunch ganja_$PRODUCT-userdebug
    elif [[ ! $PRODUCT =~ ganja_ ]]; then
        lunch ganja_$PRODUCT
    elif [[ ! $PRODUCT =~ - ]]; then
        lunch $PRODUCT-userdebug
    else
        echo "You must specify a device to build."
        exit 1
    fi
    echo; echo "Building GANJA with $THREADS threads."; echo
    make ganja -j $THREADS
}
