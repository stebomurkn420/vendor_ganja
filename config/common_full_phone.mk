##
# common stuff for all configurations
$(call inherit-product, vendor/ganja/config/common.mk)

##
# APNs copied from Pixel factory image
PRODUCT_COPY_FILES += \
    vendor/ganja/prebuilts/etc/apns-conf.xml:system/etc/apns-conf.xml

##
# Telephony packages
PRODUCT_PACKAGES += \
    Stk \
    CellBroadcastReceiver

##
# Enable tethering
PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.dun.override=0
